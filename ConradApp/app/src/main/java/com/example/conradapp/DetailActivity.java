package com.example.conradapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.conradapp.controller.FilmController;
import com.example.conradapp.model.Film;
import com.squareup.picasso.Picasso;

public class DetailActivity extends AppCompatActivity {

    FilmController controller;
    Film film;
    TextView tv_id, tv_title, tv_description, tv_mark, tv_comment, tv_year;
    ImageView iv_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        String id = getIntent().getStringExtra("idFilm");

        controller = FilmController.get(this);
        film = controller.getFilm(id);

        tv_id = findViewById(R.id.tv_id);
        tv_title = findViewById(R.id.tv_title);
        tv_description = findViewById(R.id.tv_description);
        tv_mark = findViewById(R.id.tv_mark);
        tv_comment = findViewById(R.id.tv_comment);
        tv_year = findViewById(R.id.tv_year);
        iv_image = findViewById(R.id.iv_image);

        showFilm();
    }

    private void showFilm()
    {
        tv_id.setText(film.getId());
        tv_title.setText(film.getTitle());
        tv_description.setText(film.getDescription());
        tv_mark.setText(String.valueOf(film.getMark()));
        tv_comment.setText(film.getComment());
        tv_year.setText(String.valueOf(film.getYear()));
        Picasso.get().load(film.getImage()).into(iv_image);
    }

    public void deletePressed(View view) {
        controller.deleteFilm(film);
        finish();
    }
}
