package com.example.conradapp;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.conradapp.model.Film;

import java.util.ArrayList;

public class FilmAdapter extends ArrayAdapter<Film> {
    int layoutResourceId;
    Context context;
    ArrayList<Film> data;

    public FilmAdapter(Context context, int layoutResourceId, ArrayList<Film> data){
        super(context,layoutResourceId,data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        row = inflater.inflate(layoutResourceId, parent, false);

        TextView tv_text = row.findViewById(R.id.text);
        String name = data.get(position).getTitle();
        tv_text.setText(name);

        TextView tv_text2 = row.findViewById(R.id.text2);
        String mark = String.valueOf(data.get(position).getMark());
        tv_text2.setText(mark);

        Film f = data.get(position);

        if (f.getMark()< 2){
            tv_text.setTextColor(context.getResources().getColor(R.color.colorAccent));
            tv_text2.setTextColor(context.getResources().getColor(R.color.colorAccent));
        } else if (f.getMark()==2 || f.getMark() == 3){
            tv_text.setTextColor(context.getResources().getColor(R.color.colorblack));
            tv_text2.setTextColor(context.getResources().getColor(R.color.colorblack));
        }else if (f.getMark()==4 || f.getMark() == 5){
            tv_text.setTextColor(context.getResources().getColor(R.color.colorgreen));
            tv_text2.setTextColor(context.getResources().getColor(R.color.colorgreen));
        }
        return row;
    }
}
