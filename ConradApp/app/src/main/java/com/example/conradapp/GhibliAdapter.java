package com.example.conradapp;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.conradapp.model.Ghiblifilm;

import java.util.ArrayList;


public class GhibliAdapter extends ArrayAdapter<Ghiblifilm> {

    int layoutResourceId;
    Context context;
    ArrayList<Ghiblifilm> data;

    public GhibliAdapter(Context context, int layoutResourceId, ArrayList<Ghiblifilm> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        row = inflater.inflate(layoutResourceId, parent, false);

        TextView tvTitle = row.findViewById(R.id.tvTitle);

        Ghiblifilm f = data.get(position);
        tvTitle.setText(f.getTitle());
        return row;
    }
}
