package com.example.conradapp.retrofit;

import com.example.conradapp.model.Ghiblifilm;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface MyService {

    @GET("films/")
    Call<List<Ghiblifilm>> getTitles();
}