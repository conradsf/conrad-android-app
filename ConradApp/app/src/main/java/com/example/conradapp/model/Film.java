package com.example.conradapp.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import java.util.UUID;


@Entity(tableName = "film")
public class Film {
    @PrimaryKey
    @NonNull
    String id;
    String title;
    String description;
    int mark;
    String comment;
    int year;
    String image;

    public Film() {
        id = UUID.randomUUID().toString();
    }

    public Film(String title, String description, int mark, String comment, int year, String image){
        id = UUID.randomUUID().toString();
        this.title = title;
        this.description = description;
        this.mark = mark;
        this.comment = comment;
        this.year = year;
        this.image = image;
    }
    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int vote) {
        this.mark = vote;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
