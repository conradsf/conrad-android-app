package com.example.conradapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.conradapp.controller.FilmController;
import com.example.conradapp.model.Film;
import com.example.conradapp.model.Ghiblifilm;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ListView listView;
    FilmAdapter adapter;
    ArrayList<Film> films;
    FilmController controller;
    SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.listview);
        films = new ArrayList<Film>();
        adapter = new FilmAdapter(this, R.layout.row, films);
        listView.setAdapter(adapter);
        controller = FilmController.get(this);
        prefs = getSharedPreferences("MisPreferencias", Context.MODE_PRIVATE);
            boolean isLogged = prefs.getBoolean("isLogged", false);
            if (!isLogged){
                Intent intent = new Intent(MainActivity.this, Login.class);
                startActivity(intent);
            }
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                intent.putExtra("idFilm", films.get(position).getId());
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        showFilms();
    }

    private void showFilms() {
        films.clear();
        films.addAll(controller.getFilms());
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                Intent intent = new Intent(MainActivity.this, CreateFilmActivity.class);
                startActivity(intent);
                return (true);
        }
        switch (item.getItemId()) {
            case R.id.action_get:
                Intent intent = new Intent(MainActivity.this, Ghibli.class);
                startActivity(intent);
                return (true);
        }
        return (super.onOptionsItemSelected(item));
    }
}
