package com.example.conradapp;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import com.example.conradapp.model.Ghiblifilm;
import com.example.conradapp.retrofit.MyService;
import com.example.conradapp.retrofit.RetrofitClientInstance;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Ghibli extends AppCompatActivity {
    Activity activity = this;
    ArrayList<Ghiblifilm> titles;
    ListView listView;
    GhibliAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ghibli);

        titles = new ArrayList<Ghiblifilm>();

        listView = findViewById(R.id.listview);
        adapter = new GhibliAdapter(activity, R.layout.rowghibli, titles);
        listView.setAdapter(adapter);


        getUserFromRetrofit();
    }

    private void getUserFromRetrofit() {
        MyService service = RetrofitClientInstance.getRetrofitInstance().create(MyService.class);
        Call<List<Ghiblifilm>> call = service.getTitles();

        call.enqueue(new Callback<List<Ghiblifilm>>() {
            @Override
            public void onResponse(Call<List<Ghiblifilm>> call, Response<List<Ghiblifilm>> response) {
                titles.addAll(response.body());
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Ghiblifilm>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "ERROR, Algo ha salido mal", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
