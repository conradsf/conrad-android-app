package com.example.conradapp.model;

public class Ghiblifilm {
    String title;

    public Ghiblifilm(){}

    public Ghiblifilm(String title){
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

}

