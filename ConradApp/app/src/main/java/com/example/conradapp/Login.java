package com.example.conradapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class Login extends AppCompatActivity {
    EditText etName, etPass;
    SharedPreferences prefs;
    TextView tv_url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        prefs = getSharedPreferences("MisPreferencias", Context.MODE_PRIVATE);
        etName = findViewById(R.id.etName);
        etPass = findViewById(R.id.etPass);
        tv_url = findViewById(R.id.tv_url);
        tv_url.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openBrowser();
            }
        });
    }


    public void btnSavePressed(View view) {
        if ("".equals(etName.getText().toString())) {
            etName.setError(getString(R.string.err_empty));
        } else if ("".equals(etPass.getText().toString())) {
            etPass.setError(getString(R.string.err_empty));
        } else {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("isLogged", true);
            editor.commit();
            finish();
        }
    }

    public void openBrowser() {

    Intent intent = new Intent(Intent.ACTION_VIEW);
    intent.setData(Uri.parse("https://www.abc.es/play/cine/peliculas/"));
    startActivity(intent);
    }

}

