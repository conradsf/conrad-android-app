package com.example.conradapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.conradapp.controller.FilmController;
import com.example.conradapp.model.Film;

public class CreateFilmActivity extends AppCompatActivity {

    EditText et_title, et_description, et_mark, et_comment, et_year, et_image;
    FilmController controller;
    Film film;
    String id;
    Button btn_create;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_film);

    et_title = findViewById(R.id.et_title);
    et_description = findViewById(R.id.et_description);
    et_mark = findViewById(R.id.et_mark);
    et_comment = findViewById(R.id.et_comment);
    et_year = findViewById(R.id.et_year);
    et_image = findViewById(R.id.et_image);

    controller = FilmController.get(this);
    id = getIntent().getStringExtra("idFilm");
        if (id != null){
            film = controller.getFilm(id);
            showFilm();
        }
    }

    private void showFilm() {
        et_title.setText(film.getTitle());
        et_description.setText(film.getDescription());
        et_mark.setText(String.valueOf(film.getMark()));
        et_comment.setText(film.getComment());
        et_year.setText(String.valueOf(film.getYear()));
        et_image.setText(film.getImage());

        btn_create.setText("Modificar");
    }

    public void createPressed(View view) {
        String title = et_title.getText().toString();
        String description = et_description.getText().toString();
        String mark = et_mark.getText().toString();
        String comment = et_comment.getText().toString();
        String year = et_year.getText().toString();
        String image = et_image.getText().toString();

        if (checkFields(title, description, mark, comment, year, image)) {
            if (id != null) {
                film.setTitle(title);
                film.setDescription(description);
                film.setMark(Integer.parseInt(mark));
                film.setComment(comment);
                film.setYear(Integer.parseInt(year));
                film.setImage(image);

            } else {
                Film f = new Film(title, description, Integer.parseInt(mark), comment, Integer.parseInt(year), image);
                controller.createFilm(f);

            }
            finish();
        }
    }

    private boolean checkFields(String title, String description, String mark, String comment, String year, String image) {
        String MarkPattern = "[0-5]{1}";
        boolean valid = true;

        if ("".equals(title)) {
            et_title.setError(getString(R.string.err_empty));
            valid = false;
        }
        if ("".equals(description)) {
            et_description.setError(getString(R.string.err_empty));
            valid = false;
        }
        if("".equals(mark)) {
            et_mark.setError(getString(R.string.err_empty));
            valid = false;
        }
        if ("".equals(comment)) {
            et_comment.setError(getString(R.string.err_empty));
            valid = false;
        }
        if ("".equals(year)) {
            et_year.setError(getString(R.string.err_empty));
            valid = false;
        }
        if ("".equals(image)) {
            et_image.setError(getString(R.string.err_empty));
            valid = false;
        }
        return valid;
    }
}


